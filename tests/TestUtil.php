<?php

namespace Tests;

use App\App;

class TestUtil
{

    public static function getPaymentContext(){

        $app = new App();
        return $app->getContainer()->get('App\Service\Payment\PaymentContext');
    }
}