<?php

namespace Tests\Unit;

use App\App;

class AppTest extends BaseTestCase
{
    public function testAppConstructor(){
        $app = new App();
        $this->assertInstanceOf(App::class, $app);
        return $app;
    }

}
