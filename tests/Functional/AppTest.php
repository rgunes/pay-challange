<?php

namespace Tests\Functional;

use App\App;

class AppTest extends BaseTestCase
{

    public function testGetPayAction()
    {
        $response = $this->runApp('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('<legend>Payment Page</legend>', (string)$response->getBody());
    }


}
