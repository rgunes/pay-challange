<?php

namespace Functional\Service;

use Tests\Functional\BaseTestCase;
use Tests\TestUtil;

class PaymentServiceTest extends BaseTestCase
{
    public function testPaymentSuccess()
    {
        $array = ['pay_value'=>100, 'currency'=>'TRY', 'fullname' => 'Rıza Güneş'];

        $payment = TestUtil::getPaymentContext();
        $factory = $payment->factory('Paytrek',$array);
        $this->assertTrue($factory);
        $check = $payment->checkCurreny();
        // make the necessary variables if u control dynamically
        $this->assertEquals('110', $check);
        $mailVariables = $payment->getMailVariables();
        $this->assertNotNull($mailVariables);
        $this->assertArrayHasKey('amount',$mailVariables);
        $this->assertArrayHasKey('currency',$mailVariables);
        $this->assertArrayHasKey('fullname',$mailVariables);
        $this->assertArrayHasKey('datetime',$mailVariables);

    }

    public function testPaymentFail()
    {
        $array = ['pay_value'=>100, 'currency'=>'TRY', 'fullname' => 'Rıza Güneş'];
        $payment = TestUtil::getPaymentContext();
        $this->expectException(\ErrorException::class);
        $payment->factory('PAYDAŞ',$array);

    }


}