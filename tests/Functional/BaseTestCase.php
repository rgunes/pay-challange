<?php

namespace Tests\Functional;


 use App\App;
 use PHPUnit\Framework\TestCase;
 use Slim\Http\Environment;
 use Slim\Http\Request;
 use Slim\Http\Response;

 class BaseTestCase extends TestCase
{
     /**
      * Process the application given a request method and URI
      *
      * @param string $requestMethod the request method (e.g. GET, POST, etc.)
      * @param string $requestUri the request URI
      * @param array|object|null $requestData the request data
      * @return \Psr\Http\Message\ResponseInterface
      */
     public function runApp($requestMethod, $requestUri, $requestData = null)
     {
         // Create a mock environment for testing with
         $environment = Environment::mock(
             [
                 'REQUEST_METHOD' => $requestMethod,
                 'REQUEST_URI' => $requestUri
             ]
         );

         // Set up a request object based on the environment
         $request = Request::createFromEnvironment($environment);

         // Add request data, if it exists
         if (isset($requestData)) {
             $request = $request->withParsedBody($requestData);
         }

         // Set up a response object
         $response = new Response();


         // Instantiate the application
         $app = new App();


         // Register routes
         require __DIR__ . '/../../app/routes.php';

         // Process the application
         $response = $app->process($request, $response);

         // Return the response
         return $response;
     }
}