<?php
namespace App\Service\Mail;

use Slim\Views\Twig;

class Mailer
{
    protected $view;

    protected $mailer;

    /**
     * Mailer constructor.
     * @param Twig $view
     * @param \PHPMailer $mailer
     */
    public function __construct(Twig $view, \PHPMailer $mailer)
    {
        $this->view = $view;
        //
        $this->mailer = $mailer;
        $this->mailer->SMTPDebug = false;
        $this->mailer->SMTPAuth = true;
        $this->mailer->CharSet = 'utf-8';
        $this->mailer->SMTPSecure = 'ssl';
        $this->mailer->Host = 'smtp.gmail.com';
        $this->mailer->Port = '465';
        $this->mailer->Username = 'test.rgunes@gmail.com';
        $this->mailer->Password = 'ab123654';
        $this->mailer->Mailer = 'smtp';
        $this->mailer->From = 'test.rgunes@gmail.com';
        $this->mailer->FromName = 'Rıza GÜNEŞ';
        $this->mailer->Sender = 'test.rgunes@gmail.com';
    }

    public function send($template, $data, $info)
    {
        //To Applicant
        $this->mailer->addAddress($info->email);
        $this->mailer->IsHTML(true);
        $this->mailer->Subject = $info->subject;
        $this->mailer->Body = $this->view->fetch($template, $data);
        if($this->mailer->send()){
            return true;
        }


    }
}