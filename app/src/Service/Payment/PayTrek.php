<?php

namespace App\Service\Payment;

use App\Parameters\PaymentParameters;

class PayTrek extends AbstractPayment implements PaymentInterface
{
    protected $exchangeRate = 1.10;
    protected $defaultCurrency = PaymentParameters::CURRENCY_USD;

    /**
     * @return array
     */
    function pay()
    {
        return ['success' => true];
    }

}