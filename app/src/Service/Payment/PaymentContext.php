<?php

namespace App\Service\Payment;

use App\Service\Mail\Mailer;
use Psr\Log\LoggerInterface;

class PaymentContext
{
    private $gateway = null;

    private $mailer;
    private $logger;

    /**
     * PaymentContext constructor.
     * @param Mailer $mailer
     * @param LoggerInterface $logger
     */
    public function __construct(Mailer $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param $request
     * @return bool
     */
    public function execute()
    {
        $this->checkCurreny();
        $pay = $this->gateway->pay();
        if ($pay['success']) {
            $mailVariables = $this->getMailVariables();
            return $this->sendVoucher($mailVariables);
        }
    }


    /**
     * @param $mailVariables
     * @return bool
     */
    private function sendVoucher($mailVariables)
    {
        $info = new \stdClass();
        $info->email = "riza-gunes@dataservis.org";
        $info->subject = "Payment successful";

        return $this->mailer->send('email/email.twig', $mailVariables, $info);

    }

    /**
     * @param $className
     * @param $request
     * @throws \ErrorException
     */
    public function factory($className, $request)
    {
        $className = __NAMESPACE__ . '\\' . $className;
        if (class_exists($className)) {
            $this->setGateway(new $className($request));
        } else {
            throw new \ErrorException("Cannot create new {$className} class - includes not found or class unavailable.");

        }
        return true;
    }

    /**
     * @param PaymentInterface $gateway
     */
    public function setGateway(PaymentInterface $gateway)
    {
        $this->gateway = $gateway;
    }


    /**
     * @return mixed
     */
    public function checkCurreny()
    {
        return $this->gateway->checkCurrency();
    }

    /**
     * @return array['amount','currency','fullname','datetime']
     */
    public function getMailVariables(){
        return $this->gateway->getMailVariables();
    }


}