<?php

namespace App\Service\Payment;

use App\Parameters\PaymentParameters;

class Paypal extends AbstractPayment implements PaymentInterface
{
    protected $exchangeRate = 1.08;
    protected $defaultCurrency = PaymentParameters::CURRENCY_EUR;

    /**
     * @return array
     */
    function pay()
    {
        return ['success' => true];
    }


}