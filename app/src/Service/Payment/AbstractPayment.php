<?php

namespace App\Service\Payment;

abstract class AbstractPayment
{
    protected $exchangeRate;
    protected $defaultCurrency;

    protected $amount;
    protected $currency;
    protected $fullname;

    /**
     * AbstractPayment constructor.
     * @param array $request ['currency','pay_value','fullname']
     */
    public function __construct($request)
    {
        $this->amount = $request['pay_value'];
        $this->currency = $request['currency'];
        $this->fullname = $request['fullname'];
    }

    /**
     * @param string $currency
     * @param float $amount
     */
    public function checkCurrency()
    {
        if ($this->currency !== $this->defaultCurrency) {
            return $this->amount = ($this->amount * $this->exchangeRate);
        }
    }

    /**
     * @return array['amount','currency','fullname','datetime']
     */
    public function getMailVariables()
    {
        $info = array();
        $info['amount'] = $this->amount;
        $info['currency'] = $this->currency;
        $info['fullname'] = $this->fullname;
        $datetime = new \DateTime();
        $info['datetime'] = $datetime->format('Y-m-d H:i:s');

        return $info;
    }

    /**
     * @param string $defaultCurrency
     */
    public function setDefaultCurrency($defaultCurrency)
    {
        $this->defaultCurrency = $defaultCurrency;
    }

    /**
     * @param float $exchangeRate
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    }

}