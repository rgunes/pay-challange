<?php

namespace App\Service\Payment;

use App\Parameters\PaymentParameters;

class PayU extends AbstractPayment implements PaymentInterface
{
    protected $exchangeRate = 1.12;
    protected $defaultCurrency = PaymentParameters::CURRENCY_TRY;

    /**
     * @return array
     */
    function pay()
    {
        return ['success' => true];
    }

}