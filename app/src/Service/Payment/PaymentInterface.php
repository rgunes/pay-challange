<?php
/**
 * User: rizagunes
 * Date: 03/02/2017
 * Time: 14:49
 */

namespace App\Service\Payment;


interface PaymentInterface
{
    function __construct($request);
    function checkCurrency();
    function pay();
    function getMailVariables();
}