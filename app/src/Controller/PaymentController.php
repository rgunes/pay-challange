<?php
/**
 * User: rizagunes
 * Date: 04/02/2017
 * Time: 09:40
 */

namespace App\Controller;

use App\Parameters\PaymentParameters;
use App\Service\Payment\PaymentContext;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Views\Twig as View;

final class PaymentController extends AbstractController
{
    public function getPayAction(ResponseInterface $response, View $view)
    {

        $gateways = PaymentParameters::GATEWAYS;
        $currencies = PaymentParameters::CURRENCIES;
        $view->render($response, 'home.twig', ['gateways' => $gateways, 'currencies' => $currencies]);

        return $response;
    }

       public function postPayAction(Request $request, ResponseInterface $response, PaymentContext $payment, View $view)
       {
           $gateways = PaymentParameters::GATEWAYS;
           $isValid = in_array($request->getParam('gateway'), $gateways);
           if (!$isValid) {
               throw new \ErrorException("Here comes trouble");
           }
           $payment->factory($request->getParam('gateway'), $request->getParams());
           $isPay = $payment->execute();
           return $isPay ?  "THANK U <3": "OMG!";

       }

}