<?php
/**
 * User: rizagunes
 * Date: 04/02/2017
 * Time: 09:57
 */

namespace App\Controller;


use Interop\Container\ContainerInterface;

abstract class AbstractController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    // constructor receives container instance
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __get($property)
    {
        if($this->container->{$property}){
            return $this->container->{$property};
        }
    }

}