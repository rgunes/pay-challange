<?php

namespace App;

use DI\ContainerBuilder;
use Interop\Container\ContainerInterface;

class App extends \DI\Bridge\Slim\App
{
    /**
     * @param ContainerBuilder $builder
     */
    protected function configureContainer(ContainerBuilder $builder)
    {

        $builder->addDefinitions(__DIR__ . '/../../app/settings.php');

        $definitions = [
            \Slim\Views\Twig::class => function (ContainerInterface $container) {
                $settings = $container->get('settings.view');
                $view = new \Slim\Views\Twig($settings['template_path'], $settings['twig']);

                // Add extensions
                $view->addExtension(new \Slim\Views\TwigExtension($container->get('router'), $container->get('request')->getUri()));
                $view->addExtension(new \Twig_Extension_Debug());

                return $view;
            },
            \Monolog\Logger::class => function (ContainerInterface $container) {
                $settings = $container->get('settings.logger');
                $logger = new \Monolog\Logger($settings['name']);
                $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], \Monolog\Logger::DEBUG));

                return $logger;

            },
            'mailer' => function (ContainerInterface $container) {
                $mailer = new \PHPMailer;
                return new \App\Service\Mail\Mailer($container->get('Slim\Views\Twig'), $mailer);
            },
            \App\Service\Payment\PaymentContext::class => function(ContainerInterface $container) {

                return new \App\Service\Payment\PaymentContext($container->get('mailer'), $container->get('Monolog\Logger'));
            }


            ];

        $builder->addDefinitions($definitions);
    }
}