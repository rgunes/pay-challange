<?php
namespace App\Parameters;

class PaymentParameters
{
    const GATEWAY_PAYPAL = 'Paypal';
    const GATEWAY_PAYU = 'PayU';
    const GATEWAY_PAYTREK = 'Paytrek';

    const GATEWAYS = [
        self::GATEWAY_PAYPAL,
        self::GATEWAY_PAYTREK,
        self::GATEWAY_PAYU
    ];

    const CURRENCY_EUR = 'EUR';
    const CURRENCY_TRY = 'TRY';
    const CURRENCY_USD = 'USD';

    const CURRENCIES = [
        self::CURRENCY_EUR,
        self::CURRENCY_TRY,
        self::CURRENCY_USD
    ];


}