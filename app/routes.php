<?php
// Routes

    $app->get('/', ['\App\Controller\PaymentController', 'getPayAction'])
    ->setName('getPay');

    $app->post('/', ['\App\Controller\PaymentController', 'postPayAction'])
        ->setName('postPay');
