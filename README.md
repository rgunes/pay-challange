# Pay Client

### Run it:
1. `git clone git@bitbucket.org:rgunes/pay-challange.git`
2. `composer install`
3. `$ php -S 0.0.0.0:8888 -t public public/index.php`

## Key directories

* `app`: Application code
* `app/src`: All class files within the `App` namespace
* `app/templates`: Twig template files
* `cache/twig`: Twig's Autocreated cache files
* `log`: Log files
* `public`: Webserver root
* `vendor`: Composer dependencies
* `tests`: All test class

## Key files

* `public/index.php`: Entry point to application
* `app/settings.php`: Configuration
* `app/App.php`: Kernel file and dependencies
* `app/src/Service.php`: Services for PHP-DI
* `app/src/Parameters/PaymentParameters.php`: Gateway and currency variables
* `app/routes.php`: All application routes are here